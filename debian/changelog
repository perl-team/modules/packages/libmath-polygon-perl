libmath-polygon-perl (1.10-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + libmath-polygon-perl: Add Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 13 Oct 2022 19:11:56 +0100

libmath-polygon-perl (1.10-2) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 11 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).

 -- Jelmer Vernooĳ <jelmer@debian.org>  Wed, 15 Jun 2022 19:03:08 +0100

libmath-polygon-perl (1.10-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Add debian/upstream/metadata.
  * Update years of upstream and packaging copyright.

 -- gregor herrmann <gregoa@debian.org>  Fri, 12 Jan 2018 18:56:59 +0100

libmath-polygon-perl (1.08-1) unstable; urgency=medium

  [ Damyan Ivanov ]
  * declare conformance with Policy 4.1.2 (no changes needed)

  [ Angel Abad ]
  * Import upstream version 1.08
  * debian/copyright: Update copyright years
  * Declare compliance with Debian Policy 4.1.3.
  * Bump Debhelper compat level to 11

 -- Angel Abad <angel@debian.org>  Tue, 02 Jan 2018 19:03:09 +0100

libmath-polygon-perl (1.06-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Drop fix-spelling-error patch, applied upstream.
  * Update years of upstream copyright.
  * Drop build dependency on libtest-pod-perl.
  * Declare compliance with Debian Policy 4.0.0.

 -- gregor herrmann <gregoa@debian.org>  Fri, 04 Aug 2017 17:36:51 -0400

libmath-polygon-perl (1.05-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Update patch fix-spelling-error.
    One of the two typos is fixed, the other still remains.

 -- gregor herrmann <gregoa@debian.org>  Wed, 28 Dec 2016 20:10:31 +0100

libmath-polygon-perl (1.04-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Fabrizio Regalli from Uploaders. Thanks for your work!

  [ Angel Abad ]
  * New upstream version 1.04
  * Bump Debhelper compat level 9
  * debian/copyright: Update years
  * Declare compliance with Debian policy 3.9.8
  * debian/patches/fix-spelling-error: Fix spelling error

 -- Angel Abad <angel@debian.org>  Sat, 08 Oct 2016 11:17:43 +0200

libmath-polygon-perl (1.03-1) unstable; urgency=low

  [ Angel Abad ]
  * Email change: Angel Abad -> angel@debian.org

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Florian Schlichting ]
  * Import Upstream version 1.03
  * Bump years of upstream copyright
  * Declare compliance with Debian Policy 3.9.5
  * Expand long description
  * Add myself to uploaders and copyright

 -- Florian Schlichting <fsfs@debian.org>  Fri, 07 Mar 2014 22:13:52 +0100

libmath-polygon-perl (1.02-1) unstable; urgency=low

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.

  [ Angel Abad ]
  * Imported Upstream version 1.02
  * debian/copyright:
    - Update debian/* section
    - Update upstream years
  * debian/control: Add myself to Uploaders

 -- Angel Abad <angelabad@gmail.com>  Mon, 19 Sep 2011 19:11:13 +0200

libmath-polygon-perl (1.01-2) unstable; urgency=low

  * Take over (see http://lists.debian.org/debian-perl/2011/07/msg00003.html).
  * Added Vcs-* fields in d/control.
  * Bump to 3.9.2 Standard-Version.
  * Add myself to Uploaders and Copyright.
  * Switch d/compat to 8.
  * Build-Depends: switch to debhelper (>= 8).
  * Bump to 3.0 quilt format.
  * Switch to DEP5 license format.
  * Removed unnecessary libmath-polygon-perl.docs file.

 -- Fabrizio Regalli <fabreg@fabreg.it>  Sat, 02 Jul 2011 01:41:39 +0200

libmath-polygon-perl (1.01-1) unstable; urgency=low

  * Initial Release (Closes: #577044).

 -- Martin Zobel-Helas <zobel@debian.org>  Fri, 09 Apr 2010 09:10:33 +0200
